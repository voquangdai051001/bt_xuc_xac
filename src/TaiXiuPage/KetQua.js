import React from "react";

export default function KetQua({
  luaChon,
  handlePlayGame,
  ketQua,
  soLanChoi,
  soLanThang,
}) {
  return (
    <div>
      {luaChon && <h3>Bạn chọn: {luaChon}</h3>}
      <div onClick={handlePlayGame} className="btn btn-danger">
        Play Game
      </div>
      {luaChon && <h1 className="text-danger">{ketQua}</h1>}
      <h3>Số Lần Chơi: {soLanChoi}</h3>
      <h3>Số Lần Thắng: {soLanThang}</h3>
    </div>
  );
}
