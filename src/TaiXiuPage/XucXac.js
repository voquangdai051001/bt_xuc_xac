import React from "react";
import { TAI, XIU } from "./TaiXiuPage";

export default function XucXac({ xucXacArr, handleluaChon }) {
  const renderxucXacArr = () => {
    return xucXacArr.map((item, index) => {
      return (
        <img
          key={index}
          style={{ width: "100px", margin: "10px" }}
          src={item.img}
        />
      );
    });
  };
  return (
    <div
      style={{ justifyContent: "space-between", padding: "50px" }}
      className="d-flex container"
    >
      <button
        onClick={() => {
          handleluaChon(TAI);
        }}
        className="btn btn-danger p-5"
      >
        Tài
      </button>
      <div>{renderxucXacArr()}</div>
      <button
        onClick={() => {
          handleluaChon(XIU);
        }}
        className="btn btn-danger p-5"
      >
        Xỉu
      </button>
    </div>
  );
}
