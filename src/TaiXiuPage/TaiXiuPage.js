import React, { useState } from "react";
import XucXac from "./XucXac";
import KetQua from "./KetQua";
import bg_game from "../assets/bgGame.png";
import "./game.css";
export const TAI = "TÀI";
export const XIU = "XỈU";
const WIN = "YOU WIN";
const LOSE = "YOU LOSE";

export default function TaiXiuPage() {
  const [xucXacArr, setXucXacArr] = useState([
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
  ]);
  const [ketQua, setKetQua] = useState(null);
  const [soLanChoi, setSoLanChoi] = useState(0);
  const [soLanThang, setSoLanThang] = useState(0);

  // ******

  const handlePlayGame = () => {
    let time = 3;
    let tongDiem = 0;
    // setinterval
    let count = setInterval(() => {
      time-- && renderxucXac3time();
      if (time == 0) {
        clearInterval(count);
      }
    }, 300);
    //

    // hàm tạo array mới random 3 lần

    let renderxucXac3time = () => {
      let demo = [];
      let newxucXacArr = xucXacArr.map(() => {
        let number = Math.floor(Math.random() * 6) + 1;
        demo.push(number);
        return {
          img: `./imgXucXac/${number}.png`,
          giaTri: { number },
        };
      });
      setXucXacArr(newxucXacArr);
      if (time == 0) {
        console.log("demo", demo);
        demo.forEach((element) => {
          tongDiem += element;
        });
        console.log("tongdiem", tongDiem);
        if (tongDiem >= 11 && luaChon == TAI) {
          setKetQua(WIN);
          setSoLanThang(soLanThang + 1);
        } else if (tongDiem < 11 && luaChon == XIU) {
          setKetQua(WIN);
          setSoLanThang(soLanThang + 1);
        } else {
          setKetQua(LOSE);
        }
        //
        setSoLanChoi(soLanChoi + 1);
      }
    };
    //

    // so sánh điều kiện: (tổng điểm) để quyết định thắng or thua
    // if (tongDiem >= 11 && luaChon == TAI) {
    //   setKetQua(WIN);
    //   setSoLanThang(soLanThang + 1);
    // } else if (tongDiem < 11 && luaChon == XIU) {
    //   setKetQua(WIN);
    //   setSoLanThang(soLanThang + 1);
    // } else {
    //   setKetQua(LOSE);
    // }
    // //
    // setSoLanChoi(soLanChoi + 1);
  };
  // ******

  const [luaChon, setLuaChon] = useState(null);
  const handleluaChon = (value) => {
    return setLuaChon(value);
  };
  return (
    <div
      className="bg_game
      "
      style={{ backgroundImage: `url(${bg_game})`, padding: "100px 0" }}
    >
      <div>TaiXiuPage</div>
      <XucXac xucXacArr={xucXacArr} handleluaChon={handleluaChon} />
      <KetQua
        luaChon={luaChon}
        handlePlayGame={handlePlayGame}
        ketQua={ketQua}
        soLanChoi={soLanChoi}
        soLanThang={soLanThang}
      />
    </div>
  );
}
